import UIKit

class PhoneForConnectionViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Телефон для связи"
            titleLabel.textColor = .title
        }
    }
    @IBOutlet weak var massegeLabel: UILabel!{
        didSet{
            massegeLabel.text = "Пожалуйста, введите свой номер телефона и мы его отправим владельцу автомобиля для обратной связи"
            massegeLabel.textColor = .mainTitle
        }
    }
    @IBOutlet weak var phoneForMessageField: UnderlinedTextField!{
        didSet{
            phoneForMessageField.underlineView.tintColor = .title
            phoneForMessageField.textField.textColor = .title
            phoneForMessageField.textField.keyboardType = .numberPad
            phoneForMessageField.textField.autocorrectionType = .no
            phoneForMessageField.textField.spellCheckingType = .no
            phoneForMessageField.textField.textContentType = UITextContentType.telephoneNumber
            phoneForMessageField.textField.textColor = .mainTitle
            phoneForMessageField.textField.backgroundColor = .mainBackground
            phoneForMessageField.textField.placeholder = "+7 999 999999"
        }
    }
    @IBOutlet weak var sentPhoneButton: RoundedButton!{
        didSet{
            sentPhoneButton.setTitle("Отправить номер телефона", for: UIControl.State.normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func sentNumberAsMessage(_ sender: RoundedButton) {
    }
    
}
