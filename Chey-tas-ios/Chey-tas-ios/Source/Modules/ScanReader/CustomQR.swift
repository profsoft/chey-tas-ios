import Foundation
import QRCodeReader


extension QRCodeReaderViewController {


    func addBack() {
        navigationItem.leftBarButtonItem = BarButtonItem.makeFromImage(R.image.back(),
                                                                       target: self,
                                                                       action: #selector(back))
    }

    @objc func back() {
        self.dismiss(animated: true, completion: nil)
    }

}
