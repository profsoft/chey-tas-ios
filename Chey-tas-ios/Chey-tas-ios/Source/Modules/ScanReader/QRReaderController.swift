import UIKit
import QRCodeReader
import AVFoundation
import RxSwift

class QRReaderController: UIViewController {

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        readerVC.delegate = self
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
        // Or by using the closure pattern
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in



//            self.apiClient.findByNumber(urlString: "http://cheytas-back.profsoft.online/api/login_check", number: String(subArray[subArray.count - 1]))
//                .subscribeOnDefaultOr(self.schedulerProvider.io())
//                .observeOnDefaultOr(self.schedulerProvider.main())
//                .subscribe(onNext: { (creds) in
//                    //todo
//                }, onError: { (error) in
//                    let vc = ProfileDriverViewController(
//                        nib: R.nib.profileDriverViewController
//                    )
//                    vc.otherUserId = String(subArray[subArray.count - 1])
//                    let nVC = UINavigationController(rootViewController: vc)
//                    self.present(nVC, animated: true, completion: nil)
//                }, onCompleted: {
//                    //todo
//                }, onDisposed: {
//                    //todo
//                })
//                //            .subscribeNext { creds in
//                //                let mainViewController = ChatViewController()
//                //                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
//                //                    appDelegate.router.setRoot(viewController: mainViewController)
//                //                }
//                //            }
//                .disposed(by: self.disposeBag)


        }

        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .formSheet
        readerVC.addBack()
        present(readerVC, animated: true, completion: nil)
        
    }

    @objc func back() {
        self.dismiss(animated: true, completion: nil)
    }

    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)

            // Configure the view controller (optional)
            $0.showTorchButton        = false
            $0.showSwitchCameraButton = false
            $0.showCancelButton       = false
            $0.showOverlayView        = true
            $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }

        return QRCodeReaderViewController(builder: builder)
    }()

    @IBAction func scanAction(_ sender: AnyObject) {
        // Retrieve the QRCode content
        // By using the delegate pattern
        readerVC.delegate = self

        // Or by using the closure pattern
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            print(result)
        }

        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .formSheet

        present(readerVC, animated: true, completion: nil)
    }

}

extension QRReaderController: QRCodeReaderViewControllerDelegate {

    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        let vc = ProfileDriverViewController(
            nib: R.nib.profileDriverViewController
        )
        let subArray =  result.value.split(separator: "/")
        print(subArray)
        vc.otherUserId = String(subArray[subArray.count - 1])
        let nVC = UINavigationController(rootViewController: vc)
        self.present(nVC, animated: true, completion: nil)
    }

    //This is an optional delegate method, that allows you to be notified when the user switches the cameraName
    //By pressing on the switch camera button
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        let cameraName = newCaptureDevice.device.localizedName
            print("Switching capture to: \(cameraName)")
    }

    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()

        dismiss(animated: true, completion: nil)
    }

}
