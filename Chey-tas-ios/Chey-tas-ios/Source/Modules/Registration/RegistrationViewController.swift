import UIKit
import RxSwift

class RegistrationViewController: UIViewController {

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()

    @IBOutlet weak var titleLable: UILabel!{
        didSet{
            titleLable.text = "Регистрация"
        }
    }
    @IBOutlet weak var registrationMessageLabel: UILabel!{
        didSet{
            registrationMessageLabel.tintColor = .mainTitle
            registrationMessageLabel.text = "Пожалуйста, укажите свой номер телефона"
        }
    }
    @IBOutlet weak var phoneTextField: UITextField!{
        didSet{
            
        }
    }
    @IBOutlet weak var getCodeButton: RoundedButton!{
        didSet{
            getCodeButton.setTitle("Получить код", for: UIControl.State.normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }

    @IBAction func PhoneNumberInStock(_ sender: UITextField) {
    }

    @IBAction func didTapGetButtonAction(_ sender: RoundedButton) {
        apiClient.getRegistrationCode(urlString: "http://cheytas-back.profsoft.online/api/login_check", phoneNumber: self.phoneTextField.text ?? "")
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribe(onNext: { (creds) in
                //todo
            }, onError: { (error) in
                let vc = ConfirmPhoneViewController(
                    nib: R.nib.confirmPhoneViewController
                )
                self.present(vc, animated: true, completion: nil)
            }, onCompleted: {
                //todo
            }, onDisposed: {
                //todo
            })
            .disposed(by: disposeBag)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
