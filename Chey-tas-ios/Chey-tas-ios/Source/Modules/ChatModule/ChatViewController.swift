import UIKit
import MessageKit
import MessageInputBar
import RxCocoa
import RxSwift
import Alamofire
import IQKeyboardManager

class ChatViewController: MessagesViewController {

    var messages = [Message]()

//    let socketManager = SocketManagerChat.shared

    var oldKeyboardHeight: CGFloat = 0.0

    private let disposeBag = DisposeBag()
    private let needUpdate = PublishRelay<Void>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.messagesCollectionView.shouldIgnoreScrollingAdjustment = true
        self.messagesCollectionView.layer.speed = 2.0
        self.messagesCollectionView.delegate = self
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.attributedTextMessageSizeCalculator.outgoingAvatarSize = CGSize(width: 15, height: 15)
            layout.attributedTextMessageSizeCalculator.incomingAvatarSize = CGSize(width: 15, height: 15)
            layout.photoMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.photoMessageSizeCalculator.incomingAvatarSize = .zero
        }
        self.messagesCollectionView.contentOffset = CGPoint(x: 0, y: 0)
        setupMessageCollectionView()
        setupInputBar()
        addKeyboardObservers()
        self.needUpdate.accept(())
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared().isEnabled = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnabled = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        messagesCollectionView.contentInset.bottom = messageInputBar.frame.height
        messagesCollectionView.scrollIndicatorInsets.bottom = messageInputBar.frame.height

        messagesCollectionView.scrollToBottom(animated: false)
        bindViewModel()
        self.setupInputBar()
        self.becomeFirstResponder()
    }

    func messagesUpdate() -> Observable<[Message]> {
        return Observable.just([Message(messageId: "000",
                                        sentDate: Date(),
                                        sender: Sender(id: "1",
                                                       displayName: "Sender1"),
                                        kind: .text("000"),
                                        state: 0),
                                Message(messageId: "001",
                                        sentDate: Date(),
                                        sender: Sender(id: "2",
                                                       displayName: "Sender2"),
                                        kind: .text("000"),
                                        state: 0),
                                Message(messageId: "002",
                                        sentDate: Date(),
                                        sender: Sender(id: "1",
                                                       displayName: "Sender1"),
                                        kind: .text("000"),
                                        state: 0)])
    }

    func bindViewModel() {
        messagesUpdate()
            .subscribe(onNext: { [weak self] (messages) in
                let topOutlet = self?.messagesCollectionView.contentOffset
                print(topOutlet ?? 999)
                self?.messages = messages
                self?.reloadChatCollectionView()
            })
            .disposed(by: disposeBag)
    }

    internal func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChangeState(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    internal func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidBeginEditingNotification, object: nil)
    }

    @objc
    private func textViewEditingBegin(_ notification: Notification) {
        if scrollsToBottomOnKeyboardBeginsEditing {
            guard let inputTextView = notification.object as? InputTextView, inputTextView === messageInputBar.inputTextView else { return }
            messagesCollectionView.scrollToBottom(animated: true)
        }
    }

    @objc
    private func keyboardChangeState(_ notification: Notification) {

        guard let keyboardEndFrameInScreenCoords = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
        let keyboardEndFrame = view.convert(keyboardEndFrameInScreenCoords, from: view.window)
    }

    func reloadChatCollectionView() {
        if messages.isEmpty {
            messagesCollectionView.reloadData()
        } else {
            for message in messages {
                var messageExisted = false
                for oldMessage in messages {
                    if message.messageId == oldMessage.messageId {
                        messageExisted = true
                    }
                }
                if !messageExisted {
                    insertMessage(message)
                }
            }
            messagesCollectionView.reloadData()
        }
    }

    func insertMessage(_ message: Message) {
        messages.append(message)
        // Reload last section to update header/footer labels and insert a new one
        let count = messages.count
        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([count - 1])
        }, completion: { [weak self] _ in
            guard let `self` = self else { return }
            if self.isLastSectionVisible {
                self.messagesCollectionView.scrollToBottom(animated: true)
            }
        })
    }

    @objc private func sendMessageButtonTouched() {
        let newMessage = Message(messageId: "stub",
                                 sentDate: Date(),
                                 sender: Sender(id: "1",
                                                displayName: "Sndr1"),
                                 kind: .text(messageInputBar.inputTextView.text),
                                 state: 0)
        self.messages.append(newMessage)
        self.reloadChatCollectionView()
//        let userId = UserManager.shared.currentUser?.id
//        print(userId)
//        socketManager.sendMessage(chatId: dialog?.id ?? 0,
//                                  text: messageInputBar.inputTextView.text,
//                                  replyMessage: messageToAnswer, completion: { _ in })

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.needUpdate.accept(())
        }

        messageInputBar.inputTextView.text = nil
    }

    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let messagesDataSource = messagesCollectionView.messagesDataSource,
            let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView) as? Message
            else {
                fatalError("Ouch. nil data source for messages")
        }
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)

        return cell
    }


}

extension ChatViewController: MessagesDataSource {

    func currentSender() -> Sender {
        let currentSender = "1"
        return Sender(id: "\(currentSender)", displayName: "Usr1")
    }

    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        var lastReadedMessage = 0
        for messageDTO in messages.reversed() {
            if messageDTO.state == 2 && Int(messageDTO.sender.id) == 1 {
                lastReadedMessage = Int(messageDTO.messageId) ?? 0
                break
            }
        }
        let msg = message as? Message
        if Int(msg?.sender.id ?? "0") == 1 {
            if msg?.state == 2 && Int(msg?.messageId ?? "0") == lastReadedMessage {
                avatarView.isHidden = false
            } else {
                avatarView.isHidden = true
            }
        } else {
            avatarView.isHidden = false
        }
    }

    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
}

extension ChatViewController: MessagesLayoutDelegate, MessagesDisplayDelegate {

    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 18
    }

    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 0
    }

    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 0
    }

}

private extension ChatViewController {
    var isLastSectionVisible: Bool  {
        guard !messages.isEmpty else { return false }
        let lastIndexPath = IndexPath(item: 0, section: messages.count - 1)
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }

    func setupMessageCollectionView() {
        messagesCollectionView.messagesDataSource =  self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self

        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
    }

    func setupInputBar() {

        messageInputBar.setLeftStackViewWidthConstant(to: 72, animated: false)
        //        messageInputBar.maxTextViewHeight = 100
        //        messageInputBar.shouldAutoUpdateMaxTextViewHeight = false

        messageInputBar.isTranslucent = false
        messageInputBar.inputTextView.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        messageInputBar.inputTextView.placeholderTextColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        //        messageInputBar.inputTextView.placeholder = "Сообщение..."
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 36)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 36)
        messageInputBar.inputTextView.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1).cgColor
        messageInputBar.inputTextView.layer.borderWidth = 1.0
        messageInputBar.inputTextView.layer.cornerRadius = 16.0
        messageInputBar.inputTextView.layer.masksToBounds = true
        messageInputBar.inputTextView.scrollIndicatorInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        messageInputBar.setRightStackViewWidthConstant(to: 36, animated: false)
        messageInputBar.setStackViewItems([messageInputBar.sendButton], forStack: .right, animated: true)

        // Setup send button
        messageInputBar.sendButton.contentEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        messageInputBar.sendButton.setSize(CGSize(width: 36, height: 36), animated: true)
//        messageInputBar.sendButton.image = #imageLiteral(resourceName: "SendButton")
        messageInputBar.sendButton.title = nil
        messageInputBar.sendButton.addTarget(self, action: #selector(self.sendMessageButtonTouched), for: .touchUpInside)
    }
}
