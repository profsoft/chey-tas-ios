import UIKit
import RxSwift

class SearchOwnerController: UIViewController {

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()

    @IBOutlet weak var SchImageView: UIImageView! {
        didSet {
            SchImageView.image = R.image.schImage()
        }
    }

    @IBOutlet weak var textLabel: UILabel! {
        didSet {
            textLabel.text = "Если у водителя отсутствует QR-код\nВоспользуйтесь поиском по гос. номеру"
        }
    }

    @IBOutlet weak var searchByNumberButton: RoundedButton! {
        didSet {
            searchByNumberButton.setTitle("Найти по гос. Номеру", for: .normal)
            searchByNumberButton.setTitleColor(UIColor.white, for: .normal)
            searchByNumberButton.backgroundColor = UIColor.black
        }
    }

    @IBAction func searchButtonTapped(_ sender: Any) {
        let viewController = SearchByPhoneController(
            nib: R.nib.searchByPhoneController
        )
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    @IBAction func qrButtonTapped(_ sender: Any) {
        let viewController = QRReaderController(
            nib: R.nib.qrReaderController
        )
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
        self.title = "Найти владельца"
        navigationItem.leftBarButtonItem = BarButtonItem.makeFromImage(R.image.back(),
                                                                       target: self,
                                                                       action: #selector(back))
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }

    @objc func back() {
        self.dismiss(animated: true, completion: nil)
    }

}

class BarButtonItem: UIBarButtonItem {

    override init() {
        super.init()
        setupAppearance()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAppearance()
    }

    func setupAppearance() {
        setTitleAttributesForAllStates([
            NSAttributedString.Key.foregroundColor: UIColor.black
            ])
    }

}

extension BarButtonItem {

    private var states: [UIControl.State] {
        return [.normal, .highlighted, .disabled, .focused]
    }

    func setTitleAttributesForAllStates(_ attributes: [NSAttributedString.Key: Any]) {
        states.forEach { self.setTitleTextAttributes(attributes, for: $0) }
    }

    class func makeFromImage(_ image: UIImage?,
                             title: String? = nil,
                             titleColor: UIColor? = nil,
                             font: UIFont? = nil,
                             target: Any,
                             action: Selector?) -> BarButtonItem {
        var width = CGFloat(22)
        let height = CGFloat(44)
        let maximumButtonWidth = CGFloat(150)
        let button = UIButton(type: .custom)
        button.adjustsImageWhenHighlighted = false
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        if let title = title {
            let textSidesInstets = CGFloat(8)
            let textWidth = (title as NSString).size(withAttributes: [
                NSAttributedString.Key.font: button.titleLabel?.font ?? UIFont()
                ]).width
            let textRespectingWidth = textWidth + width + 2 * textSidesInstets
            width = textRespectingWidth > maximumButtonWidth ? maximumButtonWidth : textRespectingWidth
            button.titleLabel?.lineBreakMode = .byTruncatingTail
            button.setTitleForAllStates(title)
            button.contentHorizontalAlignment = .left
            button.titleEdgeInsets = UIEdgeInsets(top: 0, left: textSidesInstets, bottom: 0, right: 0)
        }
        button.frame = CGRect(x: 0, y: 0, width: width, height: height)

        if let titleColor = titleColor {
            button.setTitleColorForAllStates(titleColor)
        }

        if let font = font {
            button.titleLabel?.font = font
        }

        if let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        return BarButtonItem(customView: button)
    }

    class func makeFromImageView(_ imageUrl: String?,
                                 target: Any,
                                 action: Selector?) -> BarButtonItem {
        let size = CGFloat(38)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        imageView.widthAnchor.constraint(equalToConstant: size).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: size).isActive = true
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        if let action = action {
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: target, action: action))
        }

        return BarButtonItem(customView: imageView)
    }

}

public extension UIButton {

    private var states: [UIControl.State] {
        return [.normal, .highlighted, .disabled, .focused]
    }

    func setTitleColorForAllStates(_ titleColor: UIColor) {
        states.forEach { self.setTitleColor(titleColor, for: $0) }
    }

    func setTitleForAllStates(_ title: String?) {
        states.forEach { self.setTitle(title, for: $0) }
    }

    func setImageForAllStates(_ image: UIImage?) {
        states.forEach { self.setImage(image, for: $0) }
    }

    func setAttributedTitleForAllStates(_ title: NSAttributedString?) {
        states.forEach { self.setAttributedTitle(title, for: $0) }
    }

}
