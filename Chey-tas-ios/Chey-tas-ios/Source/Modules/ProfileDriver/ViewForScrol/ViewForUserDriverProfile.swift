import UIKit

class ViewForUserDriverProfile: NibLoadingView {
    
    @IBOutlet weak var title: UILabel! {
        didSet {
            title.textColor = UIColor.mainTitle
        }
    }
    override func layoutSublayers(of layer: CALayer) {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.mainTitle.cgColor
        self.layer.cornerRadius = 15
    }
}
