import UIKit
import Kingfisher


class ProfileDriverViewController: UIViewController {

    var otherUserId: String?
    
    var userDriver: User {
        let user = User(name: "Василий", mobilePhone: "+7 999 99999", avatar: "", registrationNumber: "x123we34", messages: ["1", "1", "1", "1", "1"], situtions: ["1", "1", "1", "1", "1"], my: true)
        return user
    }

    @IBOutlet weak var avatarDriverImageView: UIImageView!{
        didSet{
            avatarDriverImageView.contentMode = .scaleAspectFill
            avatarDriverImageView.layer.cornerRadius = avatarDriverImageView.frame.size.width / 2
            avatarDriverImageView.clipsToBounds = true
            avatarDriverImageView.image = R.image.userIcon()
            avatarDriverImageView.tintColor = .mainTitle
        }
    }
    @IBOutlet weak var nameDriverLabel: UILabel!{
        didSet{
            nameDriverLabel.textColor = .title
            nameDriverLabel.text = "Имя"
        }
    }
    @IBOutlet weak var registrationNumberCar: UILabel!{
        didSet{
            registrationNumberCar.textColor = .mainTitle
            registrationNumberCar.text = "Рег. номер"
        }
    }
    @IBOutlet weak var scrollStatus: UIScrollView!
    
    @IBOutlet weak var messagesView: ViewForUserDriverProfile!
    @IBOutlet weak var situationView: ViewForUserDriverProfile!
    @IBOutlet weak var sentContact: ViewForUserDriverProfile!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateForUserDriver(self.userDriver)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func updateForUserDriver(_ userDriver: User) {
        avatarDriverImageView.kf.setImage(with: URL(string: "\(userDriver.avatar)"))
        nameDriverLabel.text = ""
        registrationNumberCar.text = userDriver.registrationNumber
        if otherUserId != nil {
            sentContact.isHidden = false
            messagesView.title.text = "СООБЩЕНИЯ"
            situationView.title.text = "СИТУАЦИИ"
            sentContact.title.text = "ОТПРАВИТЬ КОНТАКТ"
        } else {
            messagesView.title.text = "СООБЩЕНИЯ"
            situationView.title.text = "ШАБЛОНЫ"
            sentContact.isHidden = true
        }
    }
    
    @IBAction func massegesTapAction(_ sender: UIButton) {
        if otherUserId  != nil {
            let vc = SendMessageController(
                nib: R.nib.sendMessageController
            )
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ChatListController(
                nib: R.nib.chatListController
            )
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func situationsTapAction(_ sender: UIButton) {
        let vc = ChatListController(
            nib: R.nib.chatListController
        )
        let names: [String] = ["Ваша машина заблокировала выезд",
                               "Вашу машину поцарапали на парковке"]
        vc.section = ChatCellSectionModel(names)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func sentContactAction(_ sender: UIButton) {
        let vc = PhoneForConnectionViewController(
            nib: R.nib.phoneForConnectionViewController)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
