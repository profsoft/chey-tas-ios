import UIKit

class ThanksViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!{
        didSet{
            logoImageView.tintColor = .title
            logoImageView.image = R.image.tick()
        }
    }
    @IBOutlet weak var thanksLabel: UILabel!{
        didSet{
            thanksLabel.text = "Спаcибо!"
        }
    }
    @IBOutlet weak var ThanksMessageLabel: UILabel!{
        didSet{
            ThanksMessageLabel.text = "Владелец автомобиля получит обратную связь"
        }
    }
    @IBOutlet weak var toMainPageButton: RoundedButton!{
        didSet{
            toMainPageButton.setTitle("На главную", for: UIControl.State.normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func toMainPageActionButton(_ sender: RoundedButton) {
    }
}
