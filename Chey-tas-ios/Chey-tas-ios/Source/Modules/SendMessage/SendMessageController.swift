import UIKit
import RxSwift

class SendMessageController: UIViewController {

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Сообщения"
        }
    }
    @IBOutlet weak var messageLabel: UILabel!{
        didSet{
            messageLabel.text = "Введите нужное сообщение и мы его отправим владельцу авто"
        }
    }
    @IBOutlet weak var messageTextView: UITextView! {
        didSet {
            messageTextView.layer.cornerRadius = 10
            messageTextView.layer.borderWidth = 1
            messageTextView.layer.borderColor = UIColor(hexString: "0x9B9B9B").cgColor
        }
    }

    @IBOutlet weak var sendMessageButton: RoundedButton! {
        didSet {
            sendMessageButton.setTitle("Отправить", for: .normal)
            sendMessageButton.setTitleColor(UIColor.white, for: .normal)
            sendMessageButton.backgroundColor = UIColor.black
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }

    @IBAction func sendMessageButtonTapped(_ sender: Any) {
        self.sendMessage()
    }
    func sendMessage() {
        let creds = UserDefaults.standard.string(forKey: "bearer")
        let localId = UserDefaults.standard.integer(forKey: "localUserId")
        apiClient.sendMessage(urlString: "http://cheytas-back.profsoft.online/index.php/api/messages",
                              authBearer: creds ?? "",
                              chatId: 0,
                              car: 0,
                              text: self.messageTextView.text,
                              user: localId ?? 0)
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribe(onNext: { (creds) in
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                let nVC = UINavigationController(rootViewController: vc)
                self.present(nVC, animated: true, completion: nil)
            }, onError: { (error) in
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                self.present(vc, animated: true, completion: nil)
            }, onCompleted: {
                //todo
            }, onDisposed: {
                //todo
            })
            .disposed(by: disposeBag)
    }


}
