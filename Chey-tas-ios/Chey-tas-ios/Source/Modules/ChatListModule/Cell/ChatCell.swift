import UIKit

class ChatCell: BaseCell {
    
    @IBOutlet weak var chatLabel: UILabel!{
        didSet {
            chatLabel.textColor = UIColor.mainTitle
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
    }
    
    override func updateViews() {
        guard let model = model as? ChatCellModel else {
            return
        }
        chatLabel.text = model.name
    }
    
}
