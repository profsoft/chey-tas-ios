import Foundation

class ChatCellModel: BaseCellModel {
    typealias ActionHandler = () -> ()
    
    override var cellIdentifier: String {
        return "ChatCell"
    }
    override var cellHeight: Float {
        let heigth = Float(44 + name.count / 30 * 17)
        return heigth
    }
    
    var name: String
    
    init(_ name: String) {
        self.name = name
        
    }
}
