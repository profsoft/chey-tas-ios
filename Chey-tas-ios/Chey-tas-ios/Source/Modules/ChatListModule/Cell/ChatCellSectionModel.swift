import Foundation

protocol ChatCellSectionModelDelegate: class {
}

class ChatCellSectionModel: SectionRowsRepresentable {
    var rows: [CellIdentifiable]
    var names: [String]
    weak var delegate: ChatCellSectionModelDelegate?
    
    init(_ names: [String]) {
        self.names = names
        rows = [CellIdentifiable]()
        names.forEach { (name) in
            rows.append(ChatCellModel(name))
        }
        
    }
}
