import UIKit
import RxSwift

class ChatListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var section: ChatCellSectionModel?

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()
    
    @IBOutlet weak var nameTitleLabel: UILabel!{
        didSet{
            nameTitleLabel.text = "Ситуации"
        }
    }
    @IBOutlet weak var discriptionPageLabel: UILabel!{
        didSet{
            discriptionPageLabel.text = "Выберете нужное сообщение и мы его отправим владельцу авто"
        }
    }
    @IBOutlet weak var chatListTable: UITableView!{
        didSet{
            chatListTable.layer.borderWidth = 2
            chatListTable.layer.borderColor = UIColor.mainTitle.cgColor
            chatListTable.layer.cornerRadius = 10
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chatListTable.backgroundColor = UIColor.mainBackground
        chatListTable.dataSource = self
        chatListTable.delegate = self
        chatListTable.register(UINib(nibName: "ChatCell",
                                     bundle: nil), forCellReuseIdentifier: "ChatCell")
        guard let section = self.section else { return }
        updateForSections(section)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
    }
    func updateForSections(_ section: ChatCellSectionModel) {
        chatListTable.reloadData()
    }
    
}
extension ChatListController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.section?.rows.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = self.section?.rows[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: model?.cellIdentifier ?? "ChatCell", for: indexPath) as! BaseCell
        cell.model = model
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.section?.rows[indexPath.row].cellHeight ?? 44)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let creds = UserDefaults.standard.string(forKey: "bearer")
        let localId = UserDefaults.standard.integer(forKey: "localUserId")
        apiClient.sendMessage(urlString: "http://cheytas-back.profsoft.online/index.php/api/messages",
                              authBearer: creds ?? "",
                              chatId: 0,
                              car: 0,
                              text: self.section?.names[indexPath.row] ?? "",
                              user: localId ?? 0)
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribe(onNext: { (creds) in
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                let nVC = UINavigationController(rootViewController: vc)
                self.present(nVC, animated: true, completion: nil)
            }, onError: { (error) in
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                self.present(vc, animated: true, completion: nil)
            }, onCompleted: {
                //todo
            }, onDisposed: {
                //todo
            })
            .disposed(by: disposeBag)

    }
}
