import UIKit
import RxSwift

class ConfirmPhoneViewController: UIViewController {

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()

    @IBOutlet weak var titleLabel: UILabel!{
        didSet{
            titleLabel.text = "Подтверждение телефона"
            titleLabel.textColor = .title
        }
    }
    @IBOutlet weak var messageToUser: UILabel!{
        didSet{
            messageToUser.text = "Введите код доступа"
            messageToUser.textColor = .mainTitle
        }
    }
    @IBOutlet weak var codeViewTextField: UnderlinedTextField!{
        didSet{
            codeViewTextField.underlineView.tintColor = .title
            codeViewTextField.textField.textColor = .title
            codeViewTextField.textField.keyboardType = .numberPad
            codeViewTextField.textField.autocorrectionType = .no
            codeViewTextField.textField.spellCheckingType = .no
            codeViewTextField.textField.textContentType = UITextContentType.emailAddress
            codeViewTextField.textField.textColor = .mainTitle
            codeViewTextField.textField.backgroundColor = .mainBackground
            codeViewTextField.textField.placeholder = "654321"
        }
    }
    @IBOutlet weak var entranceButton: RoundedButton!{
        didSet{
            entranceButton.setTitle("Вход", for: UIControl.State.normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    @IBAction func entranceButtonAction(_ sender: RoundedButton) {
        apiClient.sendConfirmation(urlString: "http://cheytas-back.profsoft.online/api/login_check", confirmation: self.codeViewTextField.textField.text ?? "")
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribe(onNext: { (creds) in
                //todo
            }, onError: { (error) in
                //todo
            }, onCompleted: {
                //todo
            }, onDisposed: {
                //todo
            })
            .disposed(by: disposeBag)
    }
    
}
