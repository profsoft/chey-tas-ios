import UIKit
import RxSwift

class StartController: UIViewController {

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()

    @IBOutlet weak var backgroundImageView: UIImageView! {
        didSet {
            backgroundImageView.image = R.image.startBackground()
        }
    }

    @IBOutlet weak var loginButton: RoundedButton! {
        didSet {
            loginButton.setTitleColor(UIColor.black, for: .normal)
            loginButton.backgroundColor = UIColor.white
            loginButton.setTitle("Войти", for: .normal)
        }
    }

    @IBOutlet weak var searchButton: RoundedButton! {
        didSet {
            searchButton.setTitleColor(UIColor.white, for: .normal)
            searchButton.backgroundColor = UIColor.black
            searchButton.setTitle("Найти владельца", for: .normal)
        }
    }

    @IBAction func searchButtonTapped(_ sender: Any) {
        let viewController = SearchOwnerController(
            nib: R.nib.searchOwnerController
        )
        self.present(UINavigationController(rootViewController: viewController), animated: true, completion: nil)
    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        apiClient.auth(urlString: "http://cheytas-back.profsoft.online/api/login_check")
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribe(onNext: { (creds) in
                UserDefaults.standard.set(creds.token, forKey: "bearer")
                UserDefaults.standard.set(creds.userId, forKey: "localUserId")
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                self.navigationController?.pushViewController(vc, animated: true)
            }, onError: { (error) in
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                self.present(vc, animated: true, completion: nil)
            }, onCompleted: {
                //todo
            }, onDisposed: {
                //todo
            })
            .disposed(by: disposeBag)
    }

    @IBOutlet weak var logoImageView: UIImageView! {
        didSet {
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
    }


}
