import UIKit
import IHKeyboardAvoiding
import RxSwift

class SearchByPhoneController: UIViewController {

    var apiClient: MainApi!

    let schedulerProvider = SchedulerProviderImpl()

    let disposeBag = DisposeBag()

    @IBOutlet weak var textLabel: UILabel! {
        didSet {
            textLabel.text = "Пожалуйста, введите полностью гос. номер автомобиля чтобы связаться с владельцем"
            textLabel.textColor = UIColor(hexString: "0x9B9B9B")
        }
    }

    @IBOutlet weak var textField: UnderlinedTextField! {
        didSet {
            textField.textField.placeholder = "A 012 ВС 164"
        }
    }

    @IBOutlet weak var searchButton: RoundedButton! {
        didSet {
            searchButton.setTitle("Найти по гос. номеру", for: .normal)
            searchButton.setTitleColor(UIColor.white, for: .normal)
            searchButton.backgroundColor = UIColor.black
        }
    }

    @IBAction func searchButtonTapped(_ sender: Any) {
        apiClient.findByNumber(urlString: "http://cheytas-back.profsoft.online/api/cars.json", number: textField.textField.text ?? "")
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribe(onNext: { (creds) in
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                vc.otherUserId = String(creds.last?.id ?? 0)
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }, onError: { (error) in
                let vc = ProfileDriverViewController(
                    nib: R.nib.profileDriverViewController
                )
                vc.otherUserId = self.textField.textField.text ?? ""
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }, onCompleted: {
                //todo
            }, onDisposed: {
                //todo
            })
            .disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Найти по гос. номеру"
        apiClient = MainApiImpl(baseUrl: URL(string: "http://cheytas-back.profsoft.online")!)
        KeyboardAvoiding.avoidingView = self.searchButton
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchByPhoneController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }

     @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

}
