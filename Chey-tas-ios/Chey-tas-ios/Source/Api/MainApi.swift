import Foundation
import RxSwift
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

protocol MainApi {

    func auth(urlString: String) -> Observable<ServerCredentials>

    func sendRegNumber(urlString: String, number: String) -> Observable<ServerCredentials>

    func sendConfirmation(urlString: String, confirmation: String) -> Observable<ServerCredentials>

    func findByNumber(urlString: String, number: String) -> Observable<[ServerUser]>

    func getRegistrationCode(urlString: String, phoneNumber: String) -> Observable<ServerCredentials>

    func sendMessage(urlString: String,
                     authBearer: String,
                     chatId: Int,
                     car: Int,
                     text: String,
                     user: Int) -> Observable<ServerCredentials>

}

class MainApiImpl: BaseApi {

    var baseUrl: URL

    init(baseUrl: URL) {
        self.baseUrl = baseUrl
    }

}

extension MainApiImpl: MainApi {

    func auth(urlString: String) -> Observable<ServerCredentials> {
        return Observable.create { observer -> Disposable in
            Alamofire.request(urlString,
                              method: .post,
                              parameters: ["username": "admin",
                                           "password": "admin"],
                              encoding: JSONEncoding.default,
                              headers: nil)
                .validate()
                .responseObject { (response: DataResponse<ServerCredentials>) in
                    if let serverCredentials = response.result.value {
                        observer.onNext(serverCredentials)
                    } else {
                        observer.onError(CustomError.noData)
                    }
            }
            return Disposables.create()
        }
    }

    func sendMessage(urlString: String,
                     authBearer: String,
                     chatId: Int,
                     car: Int,
                     text: String,
                     user: Int) -> Observable<ServerCredentials> {
        return Observable.create { observer -> Disposable in
            Alamofire.request(urlString,
                              method: .post,
                              parameters: ["chatId": chatId,
                                           "car": car,
                                           "text": text,
                                           "user": user],
                              encoding: JSONEncoding.default,
                              headers: ["authBearer": authBearer])
                .validate()
                .responseObject { (response: DataResponse<ServerCredentials>) in
                    if let serverCredentials = response.result.value {
                        observer.onNext(serverCredentials)
                    } else {
                        observer.onError(CustomError.noData)
                    }
            }
            return Disposables.create()
        }
    }

    func sendRegNumber(urlString: String, number: String) -> Observable<ServerCredentials> {
        return Observable.create { observer -> Disposable in
            Alamofire.request(urlString,
                              method: .get,
                              parameters: ["username": "admin",
                                           "password": "admin"])
                .validate()
                .responseObject { (response: DataResponse<ServerCredentials>) in
                    if let serverCredentials = response.result.value {
                        observer.onNext(serverCredentials)
                    } else {
                        observer.onError(CustomError.noData)
                    }
            }
            return Disposables.create()
        }
    }

    func sendConfirmation(urlString: String, confirmation: String) -> Observable<ServerCredentials> {
        return Observable.create { observer -> Disposable in
            Alamofire.request(urlString,
                              method: .get,
                              parameters: ["username": "admin",
                                           "password": "admin"])
                .validate()
                .responseObject { (response: DataResponse<ServerCredentials>) in
                    if let serverCredentials = response.result.value {
                        observer.onNext(serverCredentials)
                    } else {
                        observer.onError(CustomError.noData)
                    }
            }
            return Disposables.create()
        }
    }

    func findByNumber(urlString: String, number: String) -> Observable<[ServerUser]> {
        return Observable.create { observer -> Disposable in
            Alamofire.request(urlString,
                              method: .get,
                              parameters: ["carNumber": number])
                .validate()
                .responseArray { (response: DataResponse<[ServerUser]>) in
                    if let serverCredentials = response.result.value {
                        observer.onNext(serverCredentials)
                    } else {
                        observer.onError(CustomError.noData)
                    }
            }
            return Disposables.create()
        }
    }

    func getRegistrationCode(urlString: String, phoneNumber: String) -> Observable<ServerCredentials> {
        return Observable.create { observer -> Disposable in
            Alamofire.request(urlString,
                              method: .get,
                              parameters: ["username": "admin",
                                           "password": "admin"])
                .validate()
                .responseObject { (response: DataResponse<ServerCredentials>) in
                    if let serverCredentials = response.result.value {
                        observer.onNext(serverCredentials)
                    } else {
                        observer.onError(CustomError.noData)
                    }
            }
            return Disposables.create()
        }
    }

}
