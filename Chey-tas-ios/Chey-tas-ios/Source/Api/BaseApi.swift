import Foundation
import RxSwift
import ObjectMapper
import Alamofire

class BaseApi {

    static let dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let dateFormat = "yyyy-MM-dd"

    var jsonDecoder: JSONDecoder {
        let decoder = JSONDecoder()
        let formatter = DateFormatter()
        formatter.dateFormat = BaseApi.dateFormat
        decoder.dateDecodingStrategy = .formatted(formatter)
        return decoder
    }

    var jsonEncoder: JSONEncoder {
        let encoder = JSONEncoder()
        let formatter = DateFormatter()
        formatter.dateFormat = BaseApi.dateFormat
        encoder.dateEncodingStrategy = .formatted(formatter)
        return encoder
    }

}

public enum CustomError: Error {
    case noData
    case faceNotFound
    case notSuccessResponse(errorCode: Int)
}

extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noData:
            return "error: no data in response"
        case .notSuccessResponse(let statusCode):
            return "error: server responded with status code \(statusCode)"
        case .faceNotFound:
            return "Лицо не найдено"
        }
    }
}

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint("=======================================")
        debugPrint(self)
        debugPrint("=======================================")
        #endif
        return self
    }
}
