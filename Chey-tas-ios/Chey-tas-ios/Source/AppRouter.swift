import UIKit
import Swinject

protocol AppRouter {
    func start()
    func setRoot(viewController: UIViewController)
}

class AppRouterImpl: AppRouter {

    let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let viewController = StartController(
            nib: R.nib.startController
        )
        setRoot(viewController: viewController)
    }

    func setRoot(viewController: UIViewController) {
        
        let nVC = UINavigationController(rootViewController: viewController)

        window.rootViewController?.dismiss(animated: false)
        window.rootViewController = nVC
        window.makeKeyAndVisible()
    }

}
