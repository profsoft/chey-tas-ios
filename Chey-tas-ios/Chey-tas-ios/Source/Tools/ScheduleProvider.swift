import Foundation
import RxSwift

protocol SchedulerProvider {

    func main() -> SchedulerType
    func io() -> SchedulerType

}

final class SchedulerProviderImpl: SchedulerProvider {

    func main() -> SchedulerType {
        return MainScheduler.instance
    }

    func io() -> SchedulerType {
        return ConcurrentDispatchQueueScheduler(qos: .background)
    }

}
