import UIKit

public extension UIFont {
    class var cellFont: UIFont {
        guard let font = UIFont(name: "DevanagariSangamMN", size: UIFont.systemFontSize) else {
            return UIFont.init()
        }
        return font
    }
}
