import Foundation
import UIKit

extension UIColor {
    class var mainBackground: UIColor {
        return UIColor.white
    }
    class var mainTitle: UIColor {
        return UIColor.gray
    }
    class var title: UIColor {
        return UIColor.black
    }
    class var mainBlue: UIColor {
        return UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
    }
}
