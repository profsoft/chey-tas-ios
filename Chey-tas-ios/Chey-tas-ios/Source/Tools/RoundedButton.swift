import UIKit

class RoundedButton: UIButton {

    var titleColor: UIColor = UIColor.white

    override func setTitleColor(_ color: UIColor?, for state: UIControl.State) {
        self.titleColor = color ?? UIColor.white
        super.setTitleColor(color, for: state)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setTitleColor(titleColor,
                           for: .normal)
        self.layer.cornerRadius = self.frame.size.height / 2
        self.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    }

}
