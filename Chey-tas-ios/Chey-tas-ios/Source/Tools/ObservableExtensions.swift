import Foundation
import RxSwift

extension Observable where E: Any {

    func switchMap<O: ObservableConvertibleType>(_ selector: @escaping (E) throws -> O) -> Observable<O.E> {
        return flatMapLatest(selector)
    }

    func subscribeOnDefaultOr(_ scheduler: ImmediateSchedulerType?) -> Observable<E> {
        guard let scheduler = scheduler else {
            return self
        }
        return self.subscribeOn(scheduler)
    }

    func observeOnDefaultOr(_ scheduler: ImmediateSchedulerType?) -> Observable<E> {
        guard let scheduler = scheduler else {
            return self
        }
        return self.observeOn(scheduler)
    }

    func subscribeNext(_ onNext: ((E) -> Void)? = nil) -> Disposable {
        return subscribe(onNext: onNext)
    }


}

func emptyOr<T>(_ observable: Observable<T>?) -> Observable<T> {
    return observable ?? Observable.empty()
}
