import UIKit

class UnderlinedTextField: NibLoadingView {

    @IBOutlet weak var textField: UITextField!

    @IBOutlet weak var underlineView: UIView! {
        didSet {
            underlineView.backgroundColor = UIColor(hexString: "0x9B9B9B")
        }
    }

    @IBOutlet weak var underlineHeightConstraint: NSLayoutConstraint! {
        didSet {
            underlineHeightConstraint.constant = 0.5
        }
    }


}
