import Foundation
import ObjectMapper

class ServerCredentials: Mappable {

    var sessionKey: String?
    var userId: Int?
    var photoUrl: String?
    var quantity: Int?
    var token: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        sessionKey    <- map["session_key"]
        userId    <- map["userid"]
        photoUrl    <- map["photo_url"]
        quantity    <- map["Quantity"]
        token    <- map["token"]
    }

}
