import Foundation
import ObjectMapper

class ServerUser: Mappable {

    var carNumber: String?
    var id: Int?
    var user: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        carNumber    <- map["carNumber"]
        id    <- map["id"]
        user    <- map["user"]
    }

}
