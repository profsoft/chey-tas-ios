import Foundation
import MessageKit

struct Message: MessageType {

    var messageId: String
    var sentDate: Date
    var sender: Sender
    var kind: MessageKind
    var state: Int

}
